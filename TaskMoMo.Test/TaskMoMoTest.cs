﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskMoMo.DAL;
using TaskMoMo.Model;
using TaskMoMo.Test.TaskMoMoService;
using System.Collections.Generic;
using System.Linq;
using TaskMoMo.Model.DTOS;

namespace TaskMoMo.Test
{
    [TestClass]
    public class TaskMoMoTest
    {
        //Global Arrange
        TaskMoMoServiceClient client = new TaskMoMoServiceClient();
        TaskMoMoDbContext context = new TaskMoMoDbContext();

        [TestMethod]
        public void Test_GetCustomer()
        {
            //Arrange
            int Id = 1;
            Customer expectedCustomer = new Customer()
            {
                Address = "San Salvador",
                CompanyName= "MOMO",
                ContactName= "Oscar",
                Country= "El Salvador",
                CustomerId=1,
                TelephoneNumber= "7118-3098"
            };

            //Act
            Customer actualCustomer = client.GetCustomer(Id);

            //Assert
            Assert.AreEqual(expectedCustomer.CustomerId,actualCustomer.CustomerId);
            Assert.AreEqual(expectedCustomer.Address, actualCustomer.Address);
            Assert.AreEqual(expectedCustomer.CompanyName, actualCustomer.CompanyName);
            Assert.AreEqual(expectedCustomer.ContactName, actualCustomer.ContactName);
            Assert.AreEqual(expectedCustomer.Country, actualCustomer.Country);
            Assert.AreEqual(expectedCustomer.TelephoneNumber, actualCustomer.TelephoneNumber);
        }

        [TestMethod]
        public void Test_GetAllCustomers()
        {
            //Arrange
            int i = 0;
            List<CustomerDto> expectedCustomer = context.Customers.Select(c => new CustomerDto()
            {
                ContactName = c.ContactName,
                CompanyName = c.CompanyName,
                TelephoneNumber = c.TelephoneNumber
            }).ToList();

            //Act
            List<CustomerDto> actualCustomer = client.GetAllCustomers().ToList();

            //Assert
            foreach (var c in actualCustomer)
            {

                Assert.AreEqual(c.CompanyName, expectedCustomer[i].CompanyName);
                Assert.AreEqual(c.ContactName, expectedCustomer[i].ContactName);
                Assert.AreEqual(c.TelephoneNumber,expectedCustomer[i].TelephoneNumber);
                i++;
            }
        }

        [TestMethod]
        public void Test_GetCustomerOrderHistory()
        {
            //Arrange
            int Id = 1;
            int i = 0;
            List<OrderDto> expectedOrders= context.Orders.Where(c => c.CustomerId == Id).Select(o => new OrderDto()
            {
                Amount = o.Amount,
                Description = o.Description
            }).ToList();

            //Act
            List<OrderDto> actualOrders= client.GetCustomerOrderHistory(Id).ToList();

            //Assert
            foreach (var o in actualOrders)
            {
                Assert.AreEqual(o.Amount, expectedOrders[i].Amount);
                Assert.AreEqual(o.Description, expectedOrders[i].Description);
                i++;
            }
        }

        [TestMethod]
        public void Test_GetCustomerOrder()
        {
            //Arrange
            int Id = 7;
            Order expectedOrder= new Order()
            {
                OrderId = Id,
                Description= "Item3",
                Date= DateTime.Parse("2017-06-19 00:00:00.000"),
                Amount = 133.2,
                Quantity =1,
                CustomerId =3
            };

            //Act
            Order actualOrder= client.GetCustomerOrder(Id);

            //Assert
            Assert.AreEqual(actualOrder.OrderId, actualOrder.OrderId);
            Assert.AreEqual(actualOrder.Description, actualOrder.Description);
            Assert.AreEqual(actualOrder.Date, actualOrder.Date);
            Assert.AreEqual(actualOrder.Amount, actualOrder.Amount);
            Assert.AreEqual(actualOrder.Quantity, actualOrder.Quantity);
            Assert.AreEqual(actualOrder.CustomerId, actualOrder.CustomerId);

        }

    }
}
