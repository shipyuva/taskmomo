﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using TaskMoMo.Model;

namespace TaskMoMo.DAL
{
    public class TaskMoMoDbContext : DbContext
    {
        public TaskMoMoDbContext() 
            : base("TaskMoMoDbContext")
        {

        }

        public DbSet<Customer> Customers{ get; set; }
        public DbSet<Order> Orders{ get; set; }
    }

    public class TaskMoMoDbInitializer:DropCreateDatabaseIfModelChanges<TaskMoMoDbContext>
    {
        protected override void Seed(TaskMoMoDbContext context)
        {
            context.Customers.Add(new Customer() { CompanyName = "MOMO", Country = "El Salvador", Address = "San Salvador", ContactName = "Oscar", TelephoneNumber = "7118-3098" });
            context.Customers.Add(new Customer() { CompanyName = "FREUND", Country = "El Salvador", Address = "LaLibertad", ContactName = "Lucy", TelephoneNumber = "2245-4400" });
            context.Customers.Add(new Customer() { CompanyName = "VIDRI", Country = "El Salvador", Address = "Tepecoyo", ContactName = "Jose", TelephoneNumber = "2252-3668" });
            context.Customers.Add(new Customer() { CompanyName = "CREATIVA", Country = "El Salvador", Address = "Santa Lucia", ContactName = "Luis", TelephoneNumber = "2277-3788" });


            context.Orders.Add(new Order() { Description = "Item111", Date = DateTime.Parse(DateTime.Today.ToShortDateString()), Amount = 10.2, Quantity = 90, CustomerId = 1 });
            context.Orders.Add(new Order() { Description = "Item16", Date = DateTime.Parse(DateTime.Today.ToShortDateString()), Amount = 12.2, Quantity = 3, CustomerId = 1 });
            context.Orders.Add(new Order() { Description = "Item16", Date = DateTime.Parse(DateTime.Today.ToShortDateString()), Amount = 13.2, Quantity = 20, CustomerId = 1 });
            context.Orders.Add(new Order() { Description = "Item15", Date = DateTime.Parse(DateTime.Today.ToShortDateString()), Amount = 13.2, Quantity = 15, CustomerId = 2 });
            context.Orders.Add(new Order() { Description = "Item14", Date = DateTime.Parse(DateTime.Today.ToShortDateString()), Amount = 15.2, Quantity = 14, CustomerId = 2 });
            context.Orders.Add(new Order() { Description = "Item4", Date = DateTime.Parse(DateTime.Today.ToShortDateString()), Amount = 11.2, Quantity = 13, CustomerId = 3 });
            context.Orders.Add(new Order() { Description = "Item3", Date = DateTime.Parse(DateTime.Today.ToShortDateString()), Amount = 133.2, Quantity = 1, CustomerId = 3 });
            context.Orders.Add(new Order() { Description = "Item2", Date = DateTime.Parse(DateTime.Today.ToShortDateString()), Amount = 144.2, Quantity = 11, CustomerId = 4 });

            context.SaveChanges();
        }
    }

}
