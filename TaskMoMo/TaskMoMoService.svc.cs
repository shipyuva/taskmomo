﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TaskMoMo.Model;
using TaskMoMo.DAL;
using TaskMoMo.Model.DTOS;

namespace TaskMoMo
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TaskMoMoService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TaskMoMoService.svc or TaskMoMoService.svc.cs at the Solution Explorer and start debugging.
    public class TaskMoMoService : ITaskMoMoService
    {
        TaskMoMoDbContext context = new TaskMoMoDbContext();

        public List<CustomerDto> GetAllCustomers()
        {
            return context.Customers.Select(c => new CustomerDto()
            {
                ContactName = c.ContactName,
                CompanyName = c.CompanyName,
                TelephoneNumber = c.TelephoneNumber
            }).ToList();

            //return context.Customers.ToList();
        }

        public Customer GetCustomer(int Id)
        {
            return context.Customers.Find(Id);
        }

        public List<OrderDto> GetCustomerOrderHistory(int Id)
        {
            //return context.Orders.Where(c=>c.CustomerId==Id).ToList();
            return context.Orders.Where(c => c.CustomerId == Id).Select(o => new OrderDto()
            {
                Amount = o.Amount,
                Description = o.Description
            }).ToList();
        }



        public Order GetCustomerOrder(int Id)
        {
            return context.Orders.Find(Id);
        }
    }
}
