﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskMoMo.Model.DTOS
{
    public class CustomerDto
    {
        public string ContactName { get; set; }

        public string CompanyName { get; set; }
        
        public string TelephoneNumber { get; set; }
    }
}