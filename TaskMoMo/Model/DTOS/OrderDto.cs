﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaskMoMo.Model.DTOS
{
    public class OrderDto
    {

        [StringLength(255)]
        public string Description { get; set; }

        public double Amount { get; set; }

    }
}