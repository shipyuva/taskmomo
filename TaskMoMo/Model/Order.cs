﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaskMoMo.Model
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }

        [StringLength(255)]
        public string Description { get; set; }


        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Date { get; set; }

        public double Amount { get; set; }

        public double Quantity { get; set; }

        
        public Customer Customer { get; set; }

        public int CustomerId { get; set; }
    }
}