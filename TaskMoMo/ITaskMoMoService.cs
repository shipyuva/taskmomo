﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TaskMoMo.Model;
using TaskMoMo.Model.DTOS;
using WCFExtras.Wsdl.Documentation;

namespace TaskMoMo
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITaskMoMoService" in both code and config file together.
    [XmlComments]
    [ServiceContract]
    public interface ITaskMoMoService
    {
        /// <summary>
        /// This is a simple operation without input paramaters.
        /// </summary>
        /// <returns>The operation returns Customer's Sumary Data Like Contact Name, Company Name and Telephone Number</returns>
        [OperationContract]
        List<CustomerDto> GetAllCustomers();

        /// <summary>
        /// This is a simple operation witch requires the Customer's Id to be used as a input paramater.
        /// </summary>
        /// <returns>The operation returns full Customer's Details</returns>
        [OperationContract]
        Customer GetCustomer(int Id);

        /// <summary>
        /// This is a simple operation witch requires the Customer's Id to be used as a input parameter.
        /// </summary>
        /// <returns>The operation returns the History of Customer's Orders</returns>
        [OperationContract]
        List<OrderDto> GetCustomerOrderHistory(int Id);

        /// <summary>
        /// This is a simple operation witch requires a Order Id to be used as a input paramaters.
        /// </summary>
        /// <returns>The operations returns a order details</returns>
        [OperationContract]
        Order GetCustomerOrder(int Id);
    }
}
